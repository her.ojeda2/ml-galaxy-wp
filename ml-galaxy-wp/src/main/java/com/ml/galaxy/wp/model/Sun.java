package com.ml.galaxy.wp.model;

public enum Sun {

	X(0),
	Y(0);
	
	double value;
	
	private Sun(double value) {
		this.value = value;
	}
	
	public double value() {
		return this.value;
	}
	
	public static Coordinate coordinate() {
		Coordinate c = new Coordinate();
		c.setX(Sun.X.value());
		c.setY(Sun.Y.value());
		return c;
	}
}
