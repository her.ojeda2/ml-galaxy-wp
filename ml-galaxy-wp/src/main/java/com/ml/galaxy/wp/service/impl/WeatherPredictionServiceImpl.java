package com.ml.galaxy.wp.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.LongStream;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import com.ml.galaxy.wp.dto.WeatherConditionReport;
import com.ml.galaxy.wp.dto.WeatherPredictionReport;
import com.ml.galaxy.wp.exception.PredictionException;
import com.ml.galaxy.wp.model.Coordinate;
import com.ml.galaxy.wp.model.Planet;
import com.ml.galaxy.wp.model.WeatherCondition;
import com.ml.galaxy.wp.model.WeatherCondition.Conditions;
import com.ml.galaxy.wp.model.WeatherPrediction;
import com.ml.galaxy.wp.process.TemporaryPredictionProcessor;
import com.ml.galaxy.wp.repo.CoordinateRepository;
import com.ml.galaxy.wp.repo.PlanetRepository;
import com.ml.galaxy.wp.repo.WeatherConditionRepository;
import com.ml.galaxy.wp.repo.WeatherPredictionRepository;
import com.ml.galaxy.wp.service.WeatherPredictionService;
import com.ml.galaxy.wp.util.PredictionUtil;

@Service
public class WeatherPredictionServiceImpl implements WeatherPredictionService{

	private static final Logger logger = LoggerFactory.getLogger(WeatherPredictionServiceImpl.class);
	
	@Autowired
	private WeatherConditionRepository weatherConditionRepository;
	
	@Autowired
	private WeatherPredictionRepository weatherPredictionRepository;
	
	@Autowired
	private ApplicationContext applicationContext;
	
	@Autowired
	private PlanetRepository planetRepository;
	
	@Autowired
	private CoordinateRepository coordinateRepository;
	
	@Autowired
	private ThreadPoolTaskExecutor temporaryPredictionExecutor;
	
	private ConcurrentMap<Conditions, WeatherCondition> availableConditions;
	
	@PostConstruct
	private void init() {
		availableConditions = new ConcurrentHashMap<Conditions, WeatherCondition>();
		weatherConditionRepository.findAll().forEach(wc -> availableConditions.put(wc.getDescription(), wc));
	}
	
	public WeatherPredictionReport executeWeatherPredictionProcess(Long years) {
		temporaryPredictionExecutor.initialize();
		truncatePredictionTables();
		logger.info("Se inicia el proceso de predicción climática.");
		Planet p1 = planetRepository.findOne(1L);
		Planet p2 = planetRepository.findOne(2L);
		Planet p3 = planetRepository.findOne(3L);
		LongStream.range(1, years + 1).forEach(i -> {
			TemporaryPredictionProcessor processor = (TemporaryPredictionProcessor) applicationContext.getBean(TemporaryPredictionProcessor.BEAN_NAME);
			processor.setYear(i);
			processor.setPlanet1(p1.clone());
			processor.setPlanet2(p2.clone());
			processor.setPlanet3(p3.clone());
			temporaryPredictionExecutor.execute(processor);
		});
		temporaryPredictionExecutor.shutdown();
		return this.generateReport();
	}
	
	public List<WeatherPrediction> predictWeatherConditionByCoordinates(Coordinate c1, Coordinate c2, Coordinate c3) throws PredictionException {
		List<WeatherPrediction> lwp = new ArrayList<WeatherPrediction>();
		WeatherCondition wc = null;
		Double trianglePerimeter = null;
		if (PredictionUtil.areAligned(c1, c2, c3)) {
			wc = availableConditions.get(PredictionUtil.linearStrategy().predictWeather(c1, c2, c3));
		} else {
			wc = availableConditions.get(PredictionUtil.triangularStrategy().predictWeather(c1, c2, c3));
			trianglePerimeter = PredictionUtil.getTrianglePerimeter(c1, c2, c3);
		}
		lwp.add(new WeatherPrediction(wc, c1, trianglePerimeter));
		lwp.add(new WeatherPrediction(wc, c2, trianglePerimeter));
		lwp.add(new WeatherPrediction(wc, c3, trianglePerimeter));
		return lwp;
	}
	
	public WeatherPredictionReport generateReport() {
		WeatherPredictionReport wpr = new WeatherPredictionReport();
		wpr.setDroughtPeriods(weatherPredictionRepository.countByCondition(Conditions.SEQUIA));
		wpr.setRainPeriods(weatherPredictionRepository.countByCondition(Conditions.LLUVIA));
		wpr.setOptimalPeriods(weatherPredictionRepository.countByCondition(Conditions.OPTIMO));
		Long lastProcessedDay = weatherPredictionRepository.lastProcessedDay();
		wpr.setProcessedYears(lastProcessedDay!=null && lastProcessedDay>0 ? lastProcessedDay/PredictionUtil.DAYS_OF_YEAR : 0L);
		if (wpr.hasData()) {
			wpr.setReport("En los " + wpr.getProcessedYears() + " procesados existen "
					+ wpr.getDroughtPeriods() + " períodos de Sequía, "
					+ wpr.getRainPeriods() + " períodos de lluvia y "
					+ wpr.getOptimalPeriods() +" períodos de óptima presión y temperatura. "
					+ " El / los días de lluvia intensa son: "  + getIntenseDaysOfRainAsString() + ".");
		} else {
			wpr.setReport("No existen datos de las predicciones meteorológicas persistidos.");
		}
		return wpr;
	}
	
	@Transactional
	public synchronized List<WeatherPrediction> saveAllPredictions(List<WeatherPrediction> predictions) {
		return weatherPredictionRepository.save(predictions);
	}
	
	public void truncatePredictionTables() {
		weatherPredictionRepository.truncate();
		coordinateRepository.truncate();
	}
	
	public WeatherConditionReport findCoordinatesByDayNumber(Long dayNumber) {
		WeatherConditionReport report = new WeatherConditionReport();
		Optional<WeatherCondition> condition = weatherPredictionRepository.findByCoordinate_Day(dayNumber).stream()
				.map(WeatherPrediction::getWeatherCondition)
				.findFirst();
		if (condition.isPresent()) {
			report.setCondition(condition.get());
			report.setReport("La condición meteorológica para el día " + dayNumber + " es " + condition.get().getDescription().name() + ".");
		} else {
			report.setReport("No existe predicción para el día seleccionado.");
		}
		return report;
	}
	
	private String getIntenseDaysOfRainAsString() {
		List<Long> days = weatherPredictionRepository.findNumberDaysByPerimeter(weatherPredictionRepository.findMaxPerimeter());
		StringBuilder sb = new StringBuilder();
		days.stream().forEach(day -> {
			sb.append(day);
			sb.append(" ");
		});
		return sb.toString();
	}
	
}
