package com.ml.galaxy.wp.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ml.galaxy.wp.model.Galaxy;

public interface GalaxyRepository extends JpaRepository<Galaxy, Long>{

}
