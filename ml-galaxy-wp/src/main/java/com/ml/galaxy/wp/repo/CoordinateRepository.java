package com.ml.galaxy.wp.repo;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.ml.galaxy.wp.model.Coordinate;

public interface CoordinateRepository extends JpaRepository<Coordinate, Long>{

	@Modifying
	@Transactional
	@Query(value="delete from coordinate", nativeQuery=true)
	public void truncate();
}
