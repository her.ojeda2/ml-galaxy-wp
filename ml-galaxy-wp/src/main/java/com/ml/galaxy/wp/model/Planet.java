package com.ml.galaxy.wp.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ml.galaxy.wp.util.PredictionUtil;
import com.ml.galaxy.wp.util.RoundingUtil;

@Entity
public class Planet implements Serializable, Cloneable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2841774854356549126L;
	
	private static final Logger logger = LoggerFactory.getLogger(Planet.class);

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	private String name;
	
	private Double sunDistance;
	
	private Double angularVelocity;
	
	private Byte turningSense; 
	
	@ManyToOne
	@JoinColumn(name="galaxy_id")
	private Galaxy galaxy;
	
	@Transient
	private Double degrees;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getSunDistance() {
		return sunDistance;
	}

	public void setSunDistance(Double sunDistance) {
		this.sunDistance = sunDistance;
	}

	public Double getAngularVelocity() {
		return angularVelocity;
	}

	public void setAngularVelocity(Double angularVelocity) {
		this.angularVelocity = angularVelocity;
	}
	
	public Galaxy getGalaxy() {
		return galaxy;
	}

	public void setGalaxy(Galaxy galaxy) {
		this.galaxy = galaxy;
	}

	public Byte getTurningSense() {
		return turningSense;
	}

	public void setTurningSense(Byte turningSense) {
		this.turningSense = turningSense;
	}
	
	public Double getDegrees() {
		return degrees;
	}

	public void setDegrees(Double degrees) {
		this.degrees = degrees;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	public Planet clone() {
		try {
			return (Planet) super.clone();
		} catch (CloneNotSupportedException e) {
			logger.error("Error al clonar el objeto Planet: ", e);
			return null;
		}
	}
	
	public void calculateInitialAngleForYear(Long year) {
		Double daysToCompleteTurning = RoundingUtil.getRoundedNumber(Coordinate.FULL_TURN / this.angularVelocity, 2);
		Double initialAngleForNextYearFromDay0 = (PredictionUtil.DAYS_OF_YEAR%daysToCompleteTurning)*angularVelocity;
		this.degrees = initialAngleForNextYearFromDay0 * (year-1);
	}
	
	public void simulateMovementDay() {
		this.degrees += (angularVelocity*turningSense);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Planet other = (Planet) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Planet [id=" + id + ", name=" + name + ", sunDistance=" + sunDistance + ", angularVelocity="
				+ angularVelocity + "]";
	}
	
}
