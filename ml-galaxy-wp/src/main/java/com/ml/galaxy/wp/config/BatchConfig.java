package com.ml.galaxy.wp.config;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;

import com.ml.galaxy.wp.task.PredictionProcessTasklet;

@Configuration
public class BatchConfig {

	private static final Logger logger = LoggerFactory.getLogger(BatchConfig.class);
	
	@Autowired
    private JobBuilderFactory jobBuilderFactory;
	
    @Autowired
    private StepBuilderFactory stepBuilderFactory;
 
	@Autowired
	@Qualifier("jobLauncher")
	private JobLauncher jobLauncher;
	
	@Scheduled(cron = "0 0 0 * * *")    
	public void sendSmsForExpiringBookmark() throws Exception {
		logger.info(" Job Started at :"+ new Date());
		JobParameters param = new JobParametersBuilder().addString("JobID",
				String.valueOf(System.currentTimeMillis())).toJobParameters();
		JobExecution execution = jobLauncher.run(job(), param);
		logger.info("Job finished with status :" + execution.getExitStatus());
	} 
	
	public Job job() {
        return jobBuilderFactory.get("jobPrediction")
                .incrementer(new RunIdIncrementer())
                .flow(step1())
                .end()
                .build();
    }
	
	public Step step1() {
        return stepBuilderFactory.get("stepPrediction")
        		.tasklet(predictionTasklet())
                .build();
    }
	
	public Tasklet predictionTasklet() {
		return new PredictionProcessTasklet();
	}
	
}
