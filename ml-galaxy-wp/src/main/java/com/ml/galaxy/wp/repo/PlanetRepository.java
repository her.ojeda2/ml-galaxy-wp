package com.ml.galaxy.wp.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ml.galaxy.wp.model.Planet;

public interface PlanetRepository extends JpaRepository<Planet, Long>{

}
