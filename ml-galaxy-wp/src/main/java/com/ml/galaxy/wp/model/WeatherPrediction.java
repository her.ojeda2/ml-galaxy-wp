package com.ml.galaxy.wp.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class WeatherPrediction implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3727485311793844618L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@ManyToOne
	@JoinColumn(name="weather_condition_id")
	private WeatherCondition weatherCondition;
	
	@OneToOne(cascade=CascadeType.PERSIST)
	@JoinColumn(name="coordinate_id")
	private Coordinate coordinate;
	
	private Double trianglePerimeter;

	public WeatherPrediction() {
		super();
	}

	public WeatherPrediction(WeatherCondition weatherCondition, Coordinate coordinate, Double trianglePerimeter) {
		super();
		this.weatherCondition = weatherCondition;
		this.coordinate = coordinate;
		this.trianglePerimeter = trianglePerimeter;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public WeatherCondition getWeatherCondition() {
		return weatherCondition;
	}

	public void setWeatherCondition(WeatherCondition weatherCondition) {
		this.weatherCondition = weatherCondition;
	}

	public Coordinate getCoordinate() {
		return coordinate;
	}

	public void setCoordinate(Coordinate coordinate) {
		this.coordinate = coordinate;
	}
	
	public Double getTrianglePerimeter() {
		return trianglePerimeter;
	}

	public void setTrianglePerimeter(Double trianglePerimeter) {
		this.trianglePerimeter = trianglePerimeter;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WeatherPrediction other = (WeatherPrediction) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "WeatherPrediction [id=" + id + ", weatherCondition=" + weatherCondition + ", coordinate=" + coordinate
				+ ", trianglePerimeter=" + trianglePerimeter + "]";
	}

}
