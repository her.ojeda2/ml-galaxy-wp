package com.ml.galaxy.wp.repo;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ml.galaxy.wp.model.WeatherCondition;
import com.ml.galaxy.wp.model.WeatherCondition.Conditions;

public interface WeatherConditionRepository extends JpaRepository<WeatherCondition, Long>{

	public Optional<WeatherCondition> findByDescription(Conditions decription);
	
}
