package com.ml.galaxy.wp.repo;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.ml.galaxy.wp.model.WeatherCondition.Conditions;
import com.ml.galaxy.wp.model.WeatherPrediction;

public interface WeatherPredictionRepository extends JpaRepository<WeatherPrediction, Long>{

	@Query("select count(*) from WeatherPrediction wp where wp.weatherCondition.description = :condition")
	public Long countByCondition(@Param("condition")Conditions condition);
	
	@Query("select max(wp.coordinate.day) from WeatherPrediction wp")
	public Long lastProcessedDay();
	
	@Query("select max(wp.trianglePerimeter) from WeatherPrediction wp")
	public Double findMaxPerimeter();  
	
	@Query("select wp.coordinate.day from WeatherPrediction wp where wp.trianglePerimeter = :perim")
	public List<Long> findNumberDaysByPerimeter(@Param("perim")Double perimeter);
	
	@Modifying
	@Transactional
	@Query(value="delete from weather_prediction", nativeQuery=true)
	public void truncate();

	public List<WeatherPrediction> findByCoordinate_Day(Long day);
}
