package com.ml.galaxy.wp.process.impl;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.LongStream;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.ml.galaxy.wp.exception.PredictionException;
import com.ml.galaxy.wp.model.Coordinate;
import com.ml.galaxy.wp.model.Planet;
import com.ml.galaxy.wp.model.WeatherPrediction;
import com.ml.galaxy.wp.process.TemporaryPredictionProcessor;
import com.ml.galaxy.wp.service.WeatherPredictionService;
import com.ml.galaxy.wp.util.PredictionUtil;

@Component(TemporaryPredictionProcessor.BEAN_NAME)
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class TemporaryPredictionProcessorImpl implements TemporaryPredictionProcessor {

	private static final Logger logger = LoggerFactory.getLogger(TemporaryPredictionProcessorImpl.class);
	
	@Autowired
	private WeatherPredictionService weatherPredictionService;
	
	private Long year;
	private Planet p1;
	private Planet p2;
	private Planet p3;
	
	private List<WeatherPrediction> predictions;
	
	@PostConstruct
	private void init() {
		predictions = new LinkedList<WeatherPrediction>();
	}
	
	@Override
	public void run() {
		logger.info("Se inicia el proceso de predicción climática para el año: " + year);
		Long from = year*PredictionUtil.DAYS_OF_YEAR-PredictionUtil.DAYS_OF_YEAR + 1;
		Long to = year*PredictionUtil.DAYS_OF_YEAR + 1;
		p1.calculateInitialAngleForYear(year);
		p2.calculateInitialAngleForYear(year);
		p3.calculateInitialAngleForYear(year);
		LongStream.range(from, to).forEach(i -> {
			Coordinate c1 = new Coordinate(p1.getDegrees(), i, p1);
			Coordinate c2 = new Coordinate(p2.getDegrees(), i, p2);
			Coordinate c3 = new Coordinate(p3.getDegrees(), i, p3);
			try {
				predictions.addAll(weatherPredictionService.predictWeatherConditionByCoordinates(c1, c2, c3));
			} catch (PredictionException e) {
				logger.error("Error en el proceso de predicción climática. ", e);
				throw new RuntimeException(e);
			}
			p1.simulateMovementDay();
			p2.simulateMovementDay();
			p3.simulateMovementDay();
		});
		weatherPredictionService.saveAllPredictions(predictions);
		logger.info("El proceso de predicción climática para el año: " + year + " ha finalizado satisfactoriamente.");
	}

	public void setYear(Long year) {
		this.year = year;
	}

	public void setPlanet1(Planet p1) {
		this.p1 = p1;
	}

	public void setPlanet2(Planet p2) {
		this.p2 = p2;
	}

	public void setPlanet3(Planet p3) {
		this.p3 = p3;
	}

}
