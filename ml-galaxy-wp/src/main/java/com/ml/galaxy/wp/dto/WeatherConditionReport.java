package com.ml.galaxy.wp.dto;

import java.io.Serializable;

import com.ml.galaxy.wp.model.WeatherCondition;

public class WeatherConditionReport implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8767992336878227090L;

	private WeatherCondition condition;
	
	private String report;

	public WeatherCondition getCondition() {
		return condition;
	}

	public void setCondition(WeatherCondition condition) {
		this.condition = condition;
	}

	public String getReport() {
		return report;
	}

	public void setReport(String report) {
		this.report = report;
	}
	
}
