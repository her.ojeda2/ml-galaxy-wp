package com.ml.galaxy.wp.exception;

public class PredictionException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6151822660236282948L;

	public PredictionException() {
		super();
	}

	public PredictionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public PredictionException(String message, Throwable cause) {
		super(message, cause);
	}

	public PredictionException(String message) {
		super(message);
	}

	public PredictionException(Throwable cause) {
		super(cause);
	}

	
}
