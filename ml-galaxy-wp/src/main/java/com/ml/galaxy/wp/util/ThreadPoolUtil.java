package com.ml.galaxy.wp.util;

import org.slf4j.Logger;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

public class ThreadPoolUtil {

	public static void waitForAvailableThread(ThreadPoolTaskExecutor taskExecutor, Long sleepTime, Logger logger) throws InterruptedException{
		for (;;) {
			int count = taskExecutor.getActiveCount();
			logger.info(logger.getName() + " -> Active Threads : " + count);
			if(count > 0) {
				if (count < taskExecutor.getCorePoolSize()) {
					break;
				} else {
					Thread.sleep(sleepTime);
				}
			} else break;
		}
	}

}
