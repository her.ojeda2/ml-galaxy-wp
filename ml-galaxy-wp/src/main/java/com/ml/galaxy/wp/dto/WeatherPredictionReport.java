package com.ml.galaxy.wp.dto;

import java.io.Serializable;

public class WeatherPredictionReport implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5387938057107457287L;

	private Long droughtPeriods;
	
	private Long rainPeriods;
	
	private Long optimalPeriods;
	
	private Long processedYears;
	
	private String report;

	public Long getDroughtPeriods() {
		return droughtPeriods;
	}

	public void setDroughtPeriods(Long droughtPeriods) {
		this.droughtPeriods = droughtPeriods;
	}

	public Long getRainPeriods() {
		return rainPeriods;
	}

	public void setRainPeriods(Long rainPeriods) {
		this.rainPeriods = rainPeriods;
	}

	public Long getOptimalPeriods() {
		return optimalPeriods;
	}

	public void setOptimalPeriods(Long optimalPeriods) {
		this.optimalPeriods = optimalPeriods;
	}

	public String getReport() {
		return report;
	}

	public void setReport(String report) {
		this.report = report;
	}

	public Long getProcessedYears() {
		return processedYears;
	}

	public void setProcessedYears(Long processedYears) {
		this.processedYears = processedYears;
	}
	
	public Boolean hasData() {
		return (droughtPeriods!=null && droughtPeriods>0) ||
					(rainPeriods!=null && rainPeriods>0) ||
						(optimalPeriods!=null && optimalPeriods>0) ||
							(processedYears!=null && processedYears>0);
	}
}
