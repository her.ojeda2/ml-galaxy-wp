package com.ml.galaxy.wp.util;

import com.ml.galaxy.wp.exception.PredictionException;
import com.ml.galaxy.wp.model.Coordinate;
import com.ml.galaxy.wp.model.WeatherCondition.Conditions;

@FunctionalInterface
public interface PredictionStrategy {
	
	public Conditions predictWeather(Coordinate c1, Coordinate c2, Coordinate c3) throws PredictionException;
	
}
