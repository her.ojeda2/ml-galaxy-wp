package com.ml.galaxy.wp.util;

import com.ml.galaxy.wp.exception.PredictionException;
import com.ml.galaxy.wp.model.Coordinate;
import com.ml.galaxy.wp.model.Sun;
import com.ml.galaxy.wp.model.WeatherCondition.Conditions;

public class PredictionUtil {

	public static final Long DAYS_OF_YEAR = 365L;
	
	public static boolean areAligned(Coordinate c1, Coordinate c2, Coordinate c3) {
		return (((c2.getX()-c1.getX())*(c3.getY()-c2.getY()))-((c2.getY()-c1.getY())*(c3.getX()-c2.getX())) == 0.00);
	}
	
	public static PredictionStrategy linearStrategy() {
		return (c1, c2, c3) -> {
			if (areAligned(c1, c2, c3)) {
				double sunX = Sun.X.value();
				double sunY = Sun.Y.value();
				if(c1.getIncline() == c2.getIncline() && c2.getIncline() == c3.getIncline() || 
						c1.getY()==sunY && c2.getY()==sunY && c3.getY()==sunY || 
						c1.getX()==sunX && c2.getX()==sunX && c3.getX()==sunX) {
					return Conditions.SEQUIA;
				} else {
					return Conditions.OPTIMO;
				}
			} else {
				throw new PredictionException("Los planetas no se encuentran alineados.");
			}
		};
	}
	
	public static PredictionStrategy triangularStrategy() {
		return (c1, c2, c3) -> {
			if (!areAligned(c1, c2, c3)) {
				if (isUnderTriangle(c1, c2, c3, Sun.coordinate())) {
					return Conditions.LLUVIA;
				} else {
					return Conditions.NORMAL;
				}
			} else {
				throw new PredictionException("Para realizar este cálculo los planetas NO deben estar alineados.");
			}
		};
	}
	
	public static Double getTrianglePerimeter(Coordinate c1, Coordinate c2, Coordinate c3) {
		Double c1c2 = getDistanceBetween(c1, c2);
		Double c1c3 = getDistanceBetween(c1, c3);
		Double c2c3 = getDistanceBetween(c2, c3);
		return RoundingUtil.getRoundedNumber(c1c2+c1c3+c2c3, 4);
	}
	
	private static Double getDistanceBetween(Coordinate c1, Coordinate c2) {
		return RoundingUtil.getRoundedNumber(Math.sqrt(Math.pow((c2.getX()-c1.getX()),2)+Math.pow((c2.getY()-c1.getY()),2)),4);
	}
	
	private static boolean isUnderTriangle(Coordinate c1, Coordinate c2, Coordinate c3, Coordinate sun) {
		Integer c1c2c3 = calculateTriangleOrientation(c1, c2, c3);
		Integer c1c2Sun = calculateTriangleOrientation(c1, c2, sun);
		Integer c1c3Sun = calculateTriangleOrientation(c1, c3, sun);
		Integer c2c3Sun = calculateTriangleOrientation(c2, c3, sun);
		return (c1c2c3<0 && c1c2Sun<0 && c1c3Sun<0 && c2c3Sun<0) || 
				(c1c2c3>0 && c1c2Sun>0 && c1c3Sun>0 && c2c3Sun>0);
	}
	
	private static Integer calculateTriangleOrientation(Coordinate c1, Coordinate c2, Coordinate c3) {
		Double orientation = (c1.getX() - c3.getX()) * (c2.getY() - c3.getY()) - (c1.getY() - c3.getY()) * (c2.getX() - c3.getX());
		return Integer.signum(orientation.intValue());
	}
	
	
}

