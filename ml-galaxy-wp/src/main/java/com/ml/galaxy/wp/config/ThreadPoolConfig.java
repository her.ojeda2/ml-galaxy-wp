package com.ml.galaxy.wp.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@Configuration
public class ThreadPoolConfig {

	public static final Integer MAX_POOL_SIZE = 10;
	
	@Bean
	public ThreadPoolTaskExecutor temporaryPredictionExecutor() {
		ThreadPoolTaskExecutor pool = new ThreadPoolTaskExecutor();
		pool.setCorePoolSize(MAX_POOL_SIZE);
		pool.setWaitForTasksToCompleteOnShutdown(true);
		pool.setAwaitTerminationSeconds(Integer.MAX_VALUE);
		return pool;
	}

}
