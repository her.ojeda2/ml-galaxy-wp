package com.ml.galaxy.wp.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class WeatherCondition implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5137433310032953352L;
	
	public enum Conditions {SEQUIA, OPTIMO, NORMAL, LLUVIA, LLUVIA_INTENSA}
	public enum ConditionType {LINEAR,TRIANGULAR}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Enumerated(EnumType.STRING)
	private Conditions description;
	
	@Enumerated(EnumType.STRING)
	private ConditionType type;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Conditions getDescription() {
		return description;
	}

	public void setDescription(Conditions description) {
		this.description = description;
	}
	
	public ConditionType getType() {
		return type;
	}

	public void setType(ConditionType type) {
		this.type = type;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WeatherCondition other = (WeatherCondition) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "WeatherCondition [id=" + id + ", description=" + description + "]";
	}
	
	
}
