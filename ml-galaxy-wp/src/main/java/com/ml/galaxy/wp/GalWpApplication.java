package com.ml.galaxy.wp;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableScheduling;

import springfox.documentation.spring.data.rest.configuration.SpringDataRestConfiguration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
@EnableBatchProcessing
@EnableScheduling
@Import(SpringDataRestConfiguration.class)
public class GalWpApplication extends SpringBootServletInitializer{

	public static void main(String[] args) {
		SpringApplication.run(GalWpApplication.class, args);
	}
}
