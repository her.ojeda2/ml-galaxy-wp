package com.ml.galaxy.wp.util;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class RoundingUtil {

	public static Double getRoundedNumber(Double number, int decimals) {
		return new BigDecimal(number).setScale(decimals, RoundingMode.HALF_UP).doubleValue();
	}
}
