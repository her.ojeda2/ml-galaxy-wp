package com.ml.galaxy.wp.task;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;

import com.ml.galaxy.wp.service.WeatherPredictionService;

public class PredictionProcessTasklet implements Tasklet{

	@Autowired
	private WeatherPredictionService weatherPredictionService;
	
	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		weatherPredictionService.executeWeatherPredictionProcess(10L);
		return RepeatStatus.FINISHED;
	}

}
