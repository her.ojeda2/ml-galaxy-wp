package com.ml.galaxy.wp.process;

import com.ml.galaxy.wp.model.Planet;

public interface TemporaryPredictionProcessor extends Runnable{
	
	public static final String BEAN_NAME = "temporaryPredictionProcessor";
	
	public void setYear(Long year);
	public void setPlanet1(Planet p1);
	public void setPlanet2(Planet p2);
	public void setPlanet3(Planet p3);
}
