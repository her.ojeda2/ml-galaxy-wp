package com.ml.galaxy.wp.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.ml.galaxy.wp.util.RoundingUtil;

@Entity
public class Coordinate implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4950638459774079371L;

	public static final Double FULL_TURN = 360D;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	private Double x;
	
	private Double y;
	
	private Double incline;
	
	private Double degrees;
	
	private Long day;
	
	@ManyToOne
	@JoinColumn(name="planet_id")
	private Planet planet;

	public Coordinate() {}
	
	public Coordinate(Double degrees, Long day, Planet planet) {
		this.day = day;
		this.planet = planet;
		this.calculateCoordinates(planet, degrees);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getX() {
		return x;
	}

	public void setX(Double x) {
		this.x = x;
	}

	public Double getY() {
		return y;
	}

	public void setY(Double y) {
		this.y = y;
	}
	
	public Double getIncline() {
		return incline;
	}

	public void setIncline(Double incline) {
		this.incline = incline;
	}
	
	public Long getDay() {
		return day;
	}

	public void setDay(Long day) {
		this.day = day;
	}

	public Planet getPlanet() {
		return planet;
	}

	public void setPlanet(Planet planet) {
		this.planet = planet;
	}

	public Double getDegrees() {
		return degrees;
	}

	public void setDegrees(Double degrees) {
		this.degrees = degrees;
	}

	public void calculateCoordinates(Planet planet, Double degrees) {
		Double angle = degrees>=FULL_TURN ? degrees-FULL_TURN : degrees;
		angle = degrees<=-FULL_TURN ? degrees+FULL_TURN : degrees;
		planet.setDegrees(angle);
		this.degrees = angle;
		this.x = RoundingUtil.getRoundedNumber(planet.getSunDistance() * Math.cos(Math.toRadians(angle)) , 4);
		this.y = RoundingUtil.getRoundedNumber(planet.getSunDistance() * Math.sin(Math.toRadians(angle)) , 4);
		if (x != 0) {
			this.incline = RoundingUtil.getRoundedNumber(this.y/this.x, 4);
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Coordinate other = (Coordinate) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Coordinate [id=" + id + ", x=" + x + ", y=" + y + ", incline=" + incline + "]";
	}

}
