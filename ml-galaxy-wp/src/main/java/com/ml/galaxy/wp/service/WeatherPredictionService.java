package com.ml.galaxy.wp.service;

import java.util.List;

import com.ml.galaxy.wp.dto.WeatherConditionReport;
import com.ml.galaxy.wp.dto.WeatherPredictionReport;
import com.ml.galaxy.wp.exception.PredictionException;
import com.ml.galaxy.wp.model.Coordinate;
import com.ml.galaxy.wp.model.WeatherPrediction;

public interface WeatherPredictionService {

	public List<WeatherPrediction> predictWeatherConditionByCoordinates(Coordinate c1, Coordinate c2, Coordinate c3) throws PredictionException;
	public List<WeatherPrediction> saveAllPredictions(List<WeatherPrediction> predictions);
	public WeatherPredictionReport executeWeatherPredictionProcess(Long years);
	public WeatherPredictionReport generateReport();
	public void truncatePredictionTables();
	public WeatherConditionReport findCoordinatesByDayNumber(Long dayNumber);
	
}
