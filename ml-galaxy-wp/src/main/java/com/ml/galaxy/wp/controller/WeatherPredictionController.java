package com.ml.galaxy.wp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ml.galaxy.wp.dto.WeatherConditionReport;
import com.ml.galaxy.wp.dto.WeatherPredictionReport;
import com.ml.galaxy.wp.service.WeatherPredictionService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/weather-prediction")
@Api(value="Servicio de predicciones meteorológicas", tags={"Predicciones meteorológicas"})
public class WeatherPredictionController {

	@Autowired
	private WeatherPredictionService weatherPredictionService;
	
	@ApiOperation(value="Ejecuta el proceso que calcula las predicciones meteorológicas.", response=WeatherPredictionReport.class)
	@RequestMapping(value = "/execute-process/", 
			method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public WeatherPredictionReport executeWeatherPredictionProcess() {
		return weatherPredictionService.executeWeatherPredictionProcess(10L);
	}
	
	@ApiOperation(value="Genera el reporte de predicciones meteorológicas.", response=WeatherPredictionReport.class)
	@RequestMapping(value = "/generate-report/", 
			method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public WeatherPredictionReport generateReport() {
		return weatherPredictionService.generateReport();
	}
	
	@ApiOperation(value="Borra las prediccciones climáticas.", response=String.class)
	@RequestMapping(value = "/delete-predictions/", 
			method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	public String deletePredictions() {
		weatherPredictionService.truncatePredictionTables();
		return "Las tablas se han borrado correctamente.";
	}
	
	@ApiOperation(value="Devuelve la condición climática del día.", response=WeatherConditionReport.class)
	@RequestMapping(value = "/climate-condition/", 
			method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public WeatherConditionReport getClimateConditionByDay(Long dayNumber) {
		return weatherPredictionService.findCoordinatesByDayNumber(dayNumber);
	}
	
}
