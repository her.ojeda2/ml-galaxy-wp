package com.ml.galaxy.wp.galwp;

import static org.hamcrest.MatcherAssert.*;

import org.hamcrest.core.Is;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.ml.galaxy.wp.repo.GalaxyRepository;
import com.ml.galaxy.wp.repo.PlanetRepository;
import com.ml.galaxy.wp.repo.WeatherConditionRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ModelInitializationTest {

	@Autowired
	private GalaxyRepository galaxyRepository;
	
	@Autowired
	private PlanetRepository planetRepository;
	
	@Autowired
	private WeatherConditionRepository weatherConditionRepository;
	
	@Test	
	public void testGalaxyInitialization() {
		assertThat(galaxyRepository.findOne(1L) != null, Is.is(true));
	}
	
	@Test
	public void testPlanetInitialization() {
		assertThat(planetRepository.findOne(1L) != null, Is.is(true));
		assertThat(planetRepository.findOne(2L) != null, Is.is(true));
		assertThat(planetRepository.findOne(3L) != null, Is.is(true));
	}
	
	@Test
	public void testWeatherConditionInitialization() {
		assertThat(weatherConditionRepository.findOne(1L) != null, Is.is(true));
		assertThat(weatherConditionRepository.findOne(2L) != null, Is.is(true));
		assertThat(weatherConditionRepository.findOne(3L) != null, Is.is(true));
		assertThat(weatherConditionRepository.findOne(4L) != null, Is.is(true));
		assertThat(weatherConditionRepository.findOne(5L) != null, Is.is(true));
	}
}
