package com.ml.galaxy.wp.galwp;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GalWpApplicationTests {

	@Test
	public void contextLoads() {
		System.out.println("The context was successfully loaded.");
	}

}
