# Memory Settings
JAVA_OPTS="-server -Xms512m -Xmx4g -XX:PermSize=1024m -Djava.awt.headless=true -Dfile.encoding=UTF-8"
 
#Using APR Native Tomcar connectors
export JAVA_OPTS="$JAVA_OPTS -Djava.library.path=/usr/local/apr/lib"
 
#Oracle Java as default, uses the serial garbage collector on the
# Full Tenured heap. The Young space is collected in parallel, but the
# Tenured is not. This means that at a time of load if a full collection
# event occurs, since the event is a 'stop-the-world' serial event then
# all application threads other than the garbage collector thread are
# taken off the CPU. This can have severe consequences if requests continue
# to accrue during these 'outage' periods. (specifically webservices, webapps)
# [Also enables adaptive sizing automatically]
export JAVA_OPTS="$JAVA_OPTS -XX:+UseParallelGC"
 
# This is interpreted as a hint to the garbage collector that pause times
# of <nnn> milliseconds or less are desired. The garbage collector will
# adjust the Java heap size and other garbage collection related parameters
# in an attempt to keep garbage collection pauses shorter than <nnn> milliseconds.
# http://java.sun.com/docs/hotspot/gc5.0/ergo5.html
export JAVA_OPTS="$JAVA_OPTS -XX:MaxGCPauseMillis=1500"
 
 
# A hint to the virtual machine that it.s desirable that not more than:
# 1 / (1 + GCTimeRation) of the application execution time be spent in
# the garbage collector.
# http://themindstorms.wordpress.com/2009/01/21/advanced-jvm-tuning-for-low-pause/
export JAVA_OPTS="$JAVA_OPTS -XX:GCTimeRatio=9"
 
 
# Disable remote (distributed) garbage collection by Java clients
# and remove ability for applications to call explicit GC collection
export JAVA_OPTS="$JAVA_OPTS -XX:+DisableExplicitGC"
 
 
# Enable remote debugging
export CATALINA_OPTS="-Xdebug -Xrunjdwp:transport=dt_socket,address=8000,server=y,suspend=n"
 
if [ "$1" != "stop" ] ; then
# Uncomment to enable jconsole remote connection management.
export JAVA_OPTS="$JAVA_OPTS -Dcom.sun.management.jmxremote=true -Dcom.sun.management.jmxremote.ssl=false -Dcom.sun.management.jmxremote.port=19989 -Dcom-Dcom.sun.management.jmxremote.local.only=false -Dcom.sun.management.jmxremote.authenticate=false -Djava.rmi.server.hostname=10.141.3.42"
fi
 
 
echo "Using CATALINA_OPTS:"
for arg in $CATALINA_OPTS
do
    echo ">> " $arg
done
echo ""
 
 
echo "Using JAVA_OPTS:"
for arg in $JAVA_OPTS
do
    echo ">> " $arg
done
echo "_______________________________________________"
echo ""

